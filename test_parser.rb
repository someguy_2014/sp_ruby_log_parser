require 'minitest/autorun'
require_relative 'parser'

describe Parser do

  describe Parser::LogFile do

    describe '.path' do

      describe 'when log file path is present in input arguments' do

        it 'returns log file path' do
          log_file_path = 'webserver.log'
          args = [log_file_path]

          _(Parser::LogFile.path(args)).must_equal log_file_path
        end
      end

      describe 'when log file path is NOT present in input arguments' do

        it 'raises error' do
          assert_raises ' Log file path is not provided correctly' do
            args = []

            Parser::LogFile.path(args)
          end
        end
      end

      describe 'when input arguments with value of nil' do

        it 'raises error' do
          assert_raises 'Log file path is not provided correctly' do
            args = nil

            Parser::LogFile.path(args)
          end
        end
      end
    end

    describe '.parse_log_line' do

      describe 'when correct line of log is present' do

        it 'parses instance of logged page class' do
          page_name    = 'about'
          page_id      = :about
          visitor_ipv4 = '016.464.657.359'
          logged_line  = "/#{page_name}/2 #{visitor_ipv4}"

          logged_page = Parser::LogFile.parse_log_line(logged_line)

          _(logged_page.id).must_equal page_id
          _(logged_page.name).must_equal page_name
          _(logged_page.visitor_ipv4).must_equal visitor_ipv4
        end
      end

      describe 'when incorrect line of log is present' do

        it 'parses instance of logged page class' do
          page_name    = ''
          page_id      = :''
          visitor_ipv4 = ''
          logged_line  = "/#{page_name}/2 #{visitor_ipv4}"

          logged_page = Parser::LogFile.parse_log_line(logged_line)

          _(logged_page.id).must_equal page_id
          _(logged_page.name).must_equal page_name
          _(logged_page.visitor_ipv4).must_be_nil
        end
      end

      describe 'when incorrect line of log is present' do

        it 'parses instance of logged page class' do
          page_name    = ''
          page_id      = :''
          visitor_ipv4 = ''
          logged_line  = "/#{page_name}/2 #{visitor_ipv4}"

          logged_page = Parser::LogFile.parse_log_line(logged_line)

          _(logged_page.id).must_equal page_id
          _(logged_page.name).must_equal page_name
          _(logged_page.visitor_ipv4).must_be_nil
        end
      end
    end
  end

  describe '.parse' do

    before(:all) do
        @log_content = [
          '/home 646.865.545.408',
          '/home 646.865.545.408',
          '/home 722.247.931.582',
          '/about/2 016.464.657.359',
          '/about/2 016.464.657.359',
          '/about 184.123.665.067'
        ]
      end

    describe 'success' do

      it 'counts visited pages' do
        tempFile = Tempfile.new('tempfile')
        tempFile.write(@log_content.join("\n"))
        tempFile.close

        logged_pages_counter = Parser::LogFile.parse(tempFile.path)

        _(logged_pages_counter.viewed_pages.sorted_by_viewed_count_desc).must_equal({ home: 3, about: 3 })
        _(logged_pages_counter.uniquely_viewed_pages.sorted_by_viewed_count_desc).must_equal({ home: 2, about: 2 })
      end
    end
  end

  describe Parser::LoggedPagesCounter do

    before(:all) do
      @logged_page          = Parser::LoggedPage.new('test', '192.168.0.1')
      @logged_pages_counter = Parser::LoggedPagesCounter.new

      5.times { @logged_pages_counter.increment!(@logged_page) }
    end

    it 'counts all viewed pages' do
      _(@logged_pages_counter.viewed_pages.sorted_by_viewed_count_desc).must_equal({ test: 5 })
    end

    it 'counts uniquely viewed pages' do
      _(@logged_pages_counter.uniquely_viewed_pages.sorted_by_viewed_count_desc).must_equal({ test: 1 })
    end
  end

  describe Parser::ViewedPagesCounter do

    before(:all) do
      @viewed_pages_counter = Parser::ViewedPagesCounter.new

      5.times { @viewed_pages_counter.increment!(:high) }
      3.times { @viewed_pages_counter.increment!(:low) }
    end

    it 'counts viewed pages ordered' do
      _(@viewed_pages_counter.sorted_by_viewed_count_desc).must_equal({ high: 5, low: 3 })
      _(@viewed_pages_counter.sorted_by_viewed_count_desc).wont_equal({ high: 3, low: 5 })
    end
  end

  describe Parser::LoggedPage do

    before(:all) do
      @logged_page = Parser::LoggedPage.new('test', '192.168.0.1')
    end

    it 'counts viewed pages ordered' do
      _(@logged_page.id).must_equal :test
      _(@logged_page.name).must_equal 'test'
      _(@logged_page.visitor_ipv4).must_equal '192.168.0.1'
    end
  end

end
