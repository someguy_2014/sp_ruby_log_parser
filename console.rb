require_relative 'parser'

begin
  log_file_path = Parser::LogFile.path(ARGV)
rescue e
  puts 'USAGE: ./parser.rb webserver.log'
  exit
end

puts Parser::LogFile.parse(log_file_path)
