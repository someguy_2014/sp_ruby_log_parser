### Run
```
ruby test_parser.rb
ruby console.rb  webserver.log
```

### Implementation
1. `console.rb` - parse the log file in `terminal` mode
2. `parser.rb` - parser module with utility module and policy classes
- `Parser` - main/top module
- `Parser::LogFile` - utility module, an entry point for the parser logic
- `Parser::LoggedPagesCounter` - policy class that increments and aggregates both `all page views` and `unique page views` counts.
- `Parser::ViewedPagesCounter` - policy class that increments and holds the incremented data per page.
- `Parser::LoggedPage` - entity class /`PORO`/ that holds data for parsed page from log.
3. `test_parser.rb` - test specs, used the `Minitest testing framework`.
4. `webserver.log` - log file with visited pages and visitors IPv4 addresses
5. `Readme.md` - this file.

### TODO
1. Rework result to not be raw hash
- ViewedPagesCounter#sorted_by_viewed_count_desc
- LoggedPagesCounter#to_s
2. Add linters for improving code quality
3. Add `test error cases` in `success/error flows`
4. Fix the git history - not all in one
