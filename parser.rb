require 'set'

module Parser

  module LogFile

    module_function

    def parse(log_file_path)
      LoggedPagesCounter.new.tap do |logged_pages_counter|
        File.foreach(log_file_path) do |logged_line|
          logged_page = parse_log_line(logged_line)

          logged_pages_counter.increment!(logged_page)
        end
      end
    end

    def parse_log_line(logged_line)
      page_path, visitor_ipv4 = logged_line.split(' ').map(&:strip)
      page_name               = page_path.split("/").map(&:strip)[1]

      LoggedPage.new(page_name, visitor_ipv4)
    end

    def path(args)
      raise 'Log file path is not provided correctly' if args.length != 1

      args[0]
    end

  end

  class LoggedPagesCounter

    attr_reader :viewed_pages, :uniquely_viewed_pages

    def initialize
      @page_name_symbol_to_set_of_ips_map = Hash.new
      @viewed_pages          = ViewedPagesCounter.new
      @uniquely_viewed_pages = ViewedPagesCounter.new
    end

    def increment!(logged_page)
      increment_most_viewed_pages!(logged_page)
      increment_uniquely_viewed_pages!(logged_page)
    end

    def to_s
      "#{viewed_pages.sorted_by_viewed_count_desc}\n#{uniquely_viewed_pages.sorted_by_viewed_count_desc}"
    end

    private

    def increment_most_viewed_pages!(logged_page)
      viewed_pages.increment!(logged_page.id)
    end

    def increment_uniquely_viewed_pages!(logged_page)
      @page_name_symbol_to_set_of_ips_map[logged_page.id] ||= Set.new
      return unless @page_name_symbol_to_set_of_ips_map[logged_page.id].add?(logged_page.visitor_ipv4)

      uniquely_viewed_pages.increment!(logged_page.id)
    end
  end

  class ViewedPagesCounter

    def initialize
      @page_id_to_page_views_count_map = Hash.new(0)
    end

    def increment!(id)
      @page_id_to_page_views_count_map[id] += 1
    end

    def sorted_by_viewed_count_desc
      Hash[@page_id_to_page_views_count_map.sort_by{|k, v| v}.reverse]
    end

  end

  class LoggedPage

    attr_reader :id, :name, :visitor_ipv4

    def initialize(name, visitor_ipv4)
      @id           = name.to_sym
      @name         = name
      @visitor_ipv4 = visitor_ipv4
    end

  end

end
